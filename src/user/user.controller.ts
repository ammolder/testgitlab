import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { User } from './entities/user.entity';
import { UserCreateDto } from './dto/user.create.dto';
import { UserService } from './user.service';
import { UserUpdateDto } from './dto/user.update.dto';
import { PaginatedData } from '../types/interface';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  async createUser(@Body() userDto: UserCreateDto): Promise<User> {
    return this.userService.createUser(userDto);
  }
  @Patch(':id')
  async updateUser(@Param('id') id: string, @Body() updateUserDto: UserUpdateDto): Promise<User> {
    return this.userService.updateUser(+id, updateUserDto);
  }
  @Delete(':id')
  async softDeleteUser(@Param('id') id: string): Promise<void> {
    await this.userService.softDeleteUser(+id);
  }
  @Get('all')
  async findAll(@Query('page') page: number = 1, @Query('limit') limit: number = 10): Promise<PaginatedData<User>> {
    return this.userService.findAllUser(page, limit);
  }
  @Get(':id')
  async getUserById(@Param('id') userId: string): Promise<User> {
    return this.userService.getUserById(+userId);
  }
}
