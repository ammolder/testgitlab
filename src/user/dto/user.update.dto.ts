import { IsString, IsOptional, Length } from 'class-validator';

export class UserUpdateDto {
  @IsOptional()
  @IsString()
  @Length(1, 30)
  firstName?: string;

  @IsOptional()
  @IsString()
  @Length(1, 30)
  lastName?: string;
}
