import { Body, Controller, Get, Post, Req, UseGuards, Headers } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { TokenResponse } from '../types/interface';
import { User } from '../user/entities/user.entity';
import { UsersAuthDto } from './dto/users.auth.dto';
import { GetUser } from '../decorator/getUser.decorator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() userDto: UsersAuthDto): Promise<TokenResponse> {
    return this.authService.login(userDto);
  }

  @Get('me')
  @UseGuards(AuthGuard('jwt'))
  async getProfile(@Headers('authorization') authorizationHeader: string): Promise<User | null> {
    return await this.authService.validateUserByToken(authorizationHeader);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('refresh-token')
  async refreshTokens(@Body('refreshToken') refreshToken: string): Promise<TokenResponse> {
    return this.authService.refreshTokens(refreshToken);
  }
}
